var http = require("http");
var fechaRequire = require("./fecha");
var fecha = new Date();
var puerto = 3000;

http.createServer(function (req, res) {
	res.writeHead(200, {"Content-Type": "text/html"});
	res.end(fechaRequire.mostrarFecha(fecha));
}).listen(puerto);

console.log("Servidor corriendo en el puerto " + puerto);