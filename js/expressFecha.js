var fechaRequire = require("./fecha");
var express = require("express");
var logger = require("morgan");
var app = express();
var fecha = new Date();
var puerto = 3000;

app.use(logger("dev"));

app.get("/", (req, res) => {
	res.send(fechaRequire.mostrarFecha(fecha))
});

app.get("/about", (req, res) => {
	res.send("Alejandro, 22 años, kebab")
});

app.listen(puerto, () => {
    console.log("Servidor corriendo en el puerto " + puerto)
});
